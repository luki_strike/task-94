import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas() {
    let div = document.getElementById("emojis");
    div.innerHTML = "";

    let banana = this.emojis.map(e => e += this.banana);

    for (let i = 0; i < banana.length; i++) {
      let para = document.createElement("p");
      para.textContent = banana[i];
      div.appendChild(para);
    }
  }
}
